"""Minesweeper game and solver."""
from __future__ import annotations

import sys
import time
import random
from pprint import pprint

import pygame as pg


class PyGame():
	"""Graphical interface."""

	def __init__(self, minesweeper: Minesweeper):
		"""Constructor.

		:param minesweeper: Minesweeper object.
		"""
		self.ms = minesweeper
		self.cell_size = 20
		self.side = self.cell_size // 6
		self.margin = 5
		self.top = 40
		self.button = self.top - self.side * 2 - self.margin * 2

		pg.init()
		pg.display.set_caption('minesweeper')
		self.size = (self.ms.size[0] * self.cell_size + 10, self.ms.size[1] * self.cell_size + 10 + self.top)
		self.screen = pg.display.set_mode(self.size)
		pg.font.init()
		self.font = pg.font.SysFont('Comic Sans MS', 15)
		self.font.set_bold(True)
		self.font_big = pg.font.SysFont('Comic Sans MS', 18)
		self.font_big.set_bold(True)
	

	def start(self):
		"""Start the game loop."""
		last_time = time.time()
		while True:
			self.event()

			now = time.time()
			self.update(now - last_time)
			last_time = now

			self.render()


	def event(self):
		"""Handle events."""
		for event in pg.event.get():
			if event.type == pg.QUIT:
				pg.quit()
				sys.exit(0)
			elif event.type == pg.MOUSEBUTTONUP:
				pos = pg.mouse.get_pos()
				if event.button == 1:
					# left click
					if (self.margin < pos[0] < self.size[0] - self.margin)\
						and (self.margin * 2 + self.top < pos[1] < self.size[1] - self.margin):
						self.ms.open((
							(pos[1] - self.margin * 2 - self.top) // self.cell_size,
							(pos[0] - self.margin) // self.cell_size))
					elif ((self.size[0] - self.button) // 2 < pos[0] < (self.size[0] + self.button) // 2)\
						and (self.margin + self.side < pos[1] < self.margin + self.side + self.button):
						self.ms = Minesweeper(self.ms.size, self.ms.bombs)
							
				elif event.button == 3:
					# right click
					self.ms.toggle_flag((
						(pos[1] - self.margin * 2 - self.top) // self.cell_size,
						(pos[0] - self.margin) // self.cell_size))
			elif event.type == pg.KEYUP:
				if event.key == pg.K_RETURN:
					self.ms = Minesweeper(self.ms.size, self.ms.bombs)
				elif event.key == pg.K_ESCAPE:
					pg.quit()
					sys.exit(0)


	def update(self, delta):
		"""Update game logic."""
		pass

	
	def render(self):
		"""Render on screen."""
		pg.draw.rect(self.screen, 
			(70, 70, 70), 
			((0, 0), self.size))

		self.draw_top()

		for x in range(0, self.ms.size[1]):
			for y in range(0, self.ms.size[0]):
				if isinstance(self.ms.cells[x][y], Minesweeper.NumCell):
					self.draw_num(
						(y * self.cell_size + self.margin, x * self.cell_size + self.margin * 2 + self.top),
						self.ms.cells[x][y])
				elif isinstance(self.ms.cells[x][y], Minesweeper.Bomb):
					self.draw_bomb(
						(y * self.cell_size + self.margin, x * self.cell_size + self.margin * 2 + self.top),
						self.ms.cells[x][y])

		if self.ms.won:
			pass


		pg.display.update()


	def draw_top(self):
		"""Draw top bar displaying different stats above the minefield."""
		pg.draw.polygon(self.screen,
			(100, 100, 100),
			[
				(self.margin, self.margin), 
				(self.size[0] - self.margin, self.margin),
				(self.size[0] - self.margin, self.margin + self.top),
				(self.margin, self.margin + self.top)
			])
		pg.draw.polygon(self.screen,
			(200, 200, 200),
			[
				(self.margin, self.margin),
				(self.size[0] - self.margin, self.margin),
				(self.size[0] - self.margin - self.top // 2, self.margin + self.top // 2),
				(self.margin + self.top // 2, self.margin + self.top // 2),
				(self.margin, self.margin + self.top)
			])
		pg.draw.rect(self.screen,
			(160, 160, 160),
			(
				(self.margin + self.side, self.margin + self.side),
				(self.size[0] - self.margin * 2 - self.side * 2, self.top - self.side * 2)
			))
		bombs_left = self.ms.bombs - sum(1 for row in self.ms.cells for cell in row if cell.flagged)
		pg.draw.rect(self.screen,
			(0, 0, 0),
			(
				(self.margin * 2 + self.side, self.margin * 2 + self.side),
				(self.font_big.size(str('000'))[0], self.top - self.side * 2 - self.margin * 2)
			))
		self.text((self.margin + 25, self.margin + self.top // 2), f'{bombs_left:3}', color=(250, 50, 50), big=True)


	def draw_num(self, pos, num: Minesweeper.NumCell):
		"""Draw an opened cell with number.

		:param pos: tuple (x, y) - coordinates on screen of the top left corner.
		:param num: NumCell object.
		"""
		if not num.opened:
			self.draw_cell(pos, num.flagged)
		else:
			pg.draw.rect(self.screen,
				(160, 160, 160),
				(pos, (self.cell_size, self.cell_size)))
			pg.draw.rect(self.screen,
				(200, 200, 200),
				((pos[0]+1, pos[1]+1), (self.cell_size-1, self.cell_size-1)))
			if num.value == 1:
				color = (0, 0, 200)
			elif num.value == 2:
				color = (0, 100, 0)
			elif num.value == 3:
				color = (200, 0, 0)
			elif num.value == 4:
				color = (100, 0, 100)
			elif num.value == 5:
				color = (200, 0, 50)
			elif num.value == 6:
				color = (0, 100, 100)
			elif num.value == 7:
				color = (0, 0, 0)
			elif num.value == 8:
				color = (100, 100, 100)
			else:
				return
			self.text((pos[0] + self.cell_size // 2, pos[1] + self.cell_size // 2), num.value, color)


	def draw_bomb(self, pos, bomb: Minesweeper.Bomb):
		"""Draw an opened cell with bomb.

		:param pos: tuple (x, y) - coordinates on screen of the top left corner.
		:param bomb: Bomb object.
		"""
		if not bomb.opened:
			self.draw_cell(pos, bomb.flagged)
		else:
			pg.draw.rect(self.screen, 
				(100, 100, 100), 
				(pos, (self.cell_size, self.cell_size)))
			pg.draw.rect(self.screen, 
				(240, 40, 40), 
				((pos[0]+1, pos[1]+1), (self.cell_size-1, self.cell_size-1)))
			pg.draw.circle(self.screen, 
				(0, 0, 0), 
				(pos[0] + self.cell_size // 2, pos[1] + self.cell_size // 2), 
				self.cell_size // 3)


	def draw_cell(self, pos, flag=False):
		"""Draw a closed cell.

		:param pos: tuple (x, y) - coordinates on screen of the top left corner.
		:param flag: whether to draw flag.
		"""
		pg.draw.rect(self.screen, 
			(200, 200, 200), 
			(pos, (self.cell_size, self.cell_size)))
		pg.draw.polygon(self.screen, 
			(100, 100, 100),
			[
				(pos[0], pos[1] + self.cell_size), 
				(pos[0] + self.cell_size, pos[1] + self.cell_size), 
				(pos[0] + self.cell_size, pos[1])
			])
		pg.draw.rect(self.screen, 
			(160, 160, 160), 
			(
				(pos[0] + self.side, pos[1] + self.side), 
				(self.cell_size - self.side*2, self.cell_size - self.side*2)
			))

		if flag:
			pg.draw.rect(self.screen,
				(0, 0, 0),
				(
					(  # top-left
						pos[0] + self.cell_size // 3,
						pos[1] + self.cell_size * 2 // 6
					),
					(  # width, height
						self.cell_size // 6,
						self.cell_size // 2
					)
				))
			pg.draw.polygon(self.screen,
				(200, 0, 0),
				[
					(
						pos[0] + self.cell_size // 3, 
						pos[1] + self.cell_size // 6
					), (
						pos[0] + self.cell_size // 3 + self.cell_size * 2 // 5,
						pos[1] + self.cell_size // 3
					), (
						pos[0] + self.cell_size // 3,
						pos[1] + self.cell_size // 2
					)
				])


	def text(self, pos, text, color=(0, 0, 0), big=False):
		"""Shortcut to draw text.

		:param pos: tuple (x, y) - coordinates on screen of the top left corner.
		:param text: text to draw.
		:param color: text color.
		"""
		if big:
			surf = self.font_big.render(str(text), True, color)
			size = self.font_big.size(str(text))
			self.screen.blit(surf, (pos[0] - size[0] // 2, pos[1] - size[1] // 2))
		else:
			surf = self.font.render(str(text), True, color)
			size = self.font.size(str(text))
			self.screen.blit(surf, (pos[0] - size[0] // 2, pos[1] - size[1] // 2))


class Minesweeper:
	"""Minesweeper game."""

	class Cell:
		"""A cell in minefield."""

		def __init__(self):
			"""Constructor."""
			self.opened = False
			self.flagged = False
			self.checked = True


	class Bomb(Cell):
		"""A cell that contains bomb."""

		def __init__(self):
			"""Constructor."""
			super().__init__()


	class NumCell(Cell):  # noqa: D205, D400
		"""A cell that has no bomb and has a value indicating the number of bombs it touches horizontally, vertically
		and diagonally.
		"""

		def __init__(self, value: int):
			"""Constructor.

			:param value: number of bombs the cell touches.
			"""
			super().__init__()
			self.value = value


	def __init__(self, size=(40, 20), bombs=100):
		"""Constructor.

		:param size: tuple (width, height) - number of cells horizontally and vertically.
		:param bombs: total number of bombs to be spawned.
		"""
		self.size = size
		self.bombs = bombs
		self.won = False

		# initialize minefield with numbers 0
		self.cells = [[Minesweeper.NumCell(0) for x in range(size[0])] for z in range(size[1])]

		# plant bombs
		for b in range(bombs):
			while True:
				row, col = random.choice(range(self.size[1])), random.choice(range(self.size[0]))
				if not isinstance(self.cells[row][col], Minesweeper.Bomb):
					self.cells[row][col] = Minesweeper.Bomb()
					# update neighboring cells
					for i in [-1, 0, 1]:
						for j in [-1, 0, 1]:
							if 0 <= row+i < self.size[1] and 0 <= col+j < self.size[0]:
								cell = self.cells[row+i][col+j]
								if not isinstance(cell, Minesweeper.Bomb):
									self.cells[row+i][col+j].value += 1
					break


	def open(self, pos):
		"""Handle request to open a cell.

		:param pos: tuple (x, y) - position of the cell on the grid.
		"""
		cell = self.cells[pos[0]][pos[1]]
		if cell.flagged:
			return
		cell.opened = True

		if isinstance(cell, Minesweeper.Bomb):
			for row in self.cells:
				for cell in row:
					cell.flagged = False
					cell.opened = True
		else:
			sys.setrecursionlimit(self.size[0] * self.size[1])
			for row in self.cells:
				for cell in row:
					cell.checked = False

			self.reveal(pos[0], pos[1])

			for row in self.cells:
				for cell in row:
					cell.checked = True


	def reveal(self, row, col):
		"""Recursively reveal cells after initial opening if current cell contains number 0.

		:param row: to which row the cell to be revealed belongs to.
		:param col: to which column the cell to be revealed belongs to.
		"""
		self.cells[row][col].checked = True
		
		if isinstance(self.cells[row][col], Minesweeper.NumCell) and self.cells[row][col].value == 0:
			for i in [-1, 0, 1]:
				for j in [-1, 0, 1]:
					if (0 <= row+i < self.size[1]) and (0 <= col+j < self.size[0]) \
							and not self.cells[row+i][col+j].flagged:
						self.cells[row+i][col+j].opened = True
						if not self.cells[row+i][col+j].checked:
							self.reveal(row+i, col+j)


	def toggle_flag(self, pos):
		"""Toogle flag status on a cell.
		
		:param pos: tuple (x, y) - position of the cell on the grid.
		"""
		if not (cell := self.cells[pos[0]][pos[1]]).opened:
			cell.flagged = not cell.flagged


def main(args):
	"""Entry point."""
	minesweeper = Minesweeper()
	game = PyGame(minesweeper)
	game.start()


if __name__ == '__main__':
	main(sys.argv[1:])